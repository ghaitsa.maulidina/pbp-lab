import 'package:flutter/material.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Color(0xff001D3D),
            child: Text(
              'SlowLab',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Colors.white),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Home', Icons.home_filled, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Forum', Icons.question_answer, () {
          }),
          buildListTile('Lokasi Tes', Icons.location_on, () {
          }),
          buildListTile('Cek Hasil', Icons.document_scanner_rounded, () {
          }),
          buildListTile('Product List', Icons.list_alt, () {
          }),
          buildListTile('Booking', Icons.event_busy, () {
          }),
        ],
      ),
    );
  }
}
