import 'package:flutter/foundation.dart';

class Meal {
  final String id;
  final List<String> categories;
  final String title;
  final String pertanyaan;
  final String jawaban;
  final String sender;

  const Meal({
    @required this.id,
    @required this.categories,
    @required this.title,
    @required this.pertanyaan,
    @required this.sender,
    @required this.jawaban,
  });
}
