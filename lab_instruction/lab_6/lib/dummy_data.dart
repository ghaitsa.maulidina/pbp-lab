import 'package:flutter/material.dart';

import './models/category.dart';
import './models/meal.dart';

const DUMMY_CATEGORIES = const [
  Category(
    id: 'c1',
    title: 'Forum',
    color: Colors.purple,
  ),
  Category(
    id: 'c2',
    title: 'Lokasi Tes COVID-19',
    color: Colors.red,
  ),
  Category(
    id: 'c3',
    title: 'Cek Hasil',
    color: Colors.orange,
  ),
  Category(
    id: 'c4',
    title: 'Product List',
    color: Colors.amber,
  ),
  Category(
    id: 'c5',
    title: 'Booking',
    color: Colors.blue,
  ),
];

const DUMMY_MEALS = const [
  Meal(
    id: 'm1',
    categories: [
      'c1',
      'c2',
      'c3',
      'c4',
      'c5',
    ],
    title: 'Spaghetti with Tomato Sauce',
    pertanyaan: 'Berapa kah harga test PCR?',
    sender : 'Amakuno',

    jawaban: 'Maaf pertanyaan ini belum memiliki jawaban.',
  ),
  Meal(
    id: 'm2',
    categories: [
      'c1',
      'c2',
      'c3',
      'c4',
      'c5',    ],
    sender : 'Amakuno',
    title: 'Toast Hawaii',
    pertanyaan: 'Berapa kah harga test PCR?',
    jawaban: 'Maaf pertanyaan ini belum memiliki jawaban.',

  ),
  Meal(
    id: 'm3',
    categories: [
      'c1',
      'c2',
      'c3',
      'c4',
      'c5',
    ],
    sender : 'Amakuno',
    title: 'Classic Hamburger',
    pertanyaan: 'Berapa kah harga test PCR?',
    jawaban: 'Maaf pertanyaan ini belum memiliki jawaban.',
  ),
  Meal(
    id: 'm4',
    categories: [
      'c1',
      'c2',
      'c3',
      'c4',
      'c5',
    ],
    sender : 'Amakuno',
    title: 'Wiener Schnitzel',
    pertanyaan: 'Berapa kah harga test PCR?',
    jawaban: 'Maaf pertanyaan ini belum memiliki jawaban.',
  ),
  Meal(
    id: 'm5',
    categories: [
      'c1',
      'c2',
      'c3',
      'c4',
      'c5',
    ],
    sender : 'Amakuno',
    title: 'Salad with Smoked Salmon',
    pertanyaan: 'Berapa kah harga test PCR?',
    jawaban: 'Maaf pertanyaan ini belum memiliki jawaban.',
  ),
];
