from django.db import models

# Create your models here.
class Note(models.Model):
    untuk = models.CharField(max_length=20, default="")
    dari = models.CharField(max_length=20, default="")
    judul = models.CharField(max_length=30, default="")
    pesan = models.TextField(default="")