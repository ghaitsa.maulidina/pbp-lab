Apakah perbedaan antara JSON dan XML?
JSON:
- Singkatan dari JavaScript Object Notation
- Berdasarkan bahasa Javascript
- Untuk merepresentasikan object
- Tidak support namespaces
- Support array
- Formatnya lebih mudah dibaca dibanding XML
- Tidak butuh end tag
- Tidak support comment
- Hanya support UTF-8 encoding
XML:
- Singkatan dari Extensible Markup Language
- Berdasarkan SGML
- Menggunakan tag structure untuk merepresentasikan item pada data
- Tidak support array
- Lebih sulit dibaca dibandingkan JSON
- Memiliki start dan end tag
- Support comment
- Support berbagai macam encoding

Apakah perbedaan antara HTML dan XML?
HTML:
- Tidak case sensitive
- Memiliki tag yang sudah ada sebelumnya (predefined)
- Tidak selalu butuh closing tag
- Mengabaikan whitespace tambahan
- Untuk mendisplay data
XML:
- Case sensitive
- Tag harus dibuat oleh sang programmer terlebih dahulu
- Wajib menggunakan closing tag
- Mampu mempertahankan whitespace yang kita tulis
- Untuk mentransfer data